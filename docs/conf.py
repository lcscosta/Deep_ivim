"""Sphinx configuration."""
project = "Deep IVIM"
author = "Lucas da Costa"
copyright = "2022, Lucas da Costa"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_click",
    "myst_parser",
]
autodoc_typehints = "description"
html_theme = "furo"
